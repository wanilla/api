
import time
import requests
import db
import math
import re
from openpyxl import load_workbook
from copy import copy

def load_from_xlsx(filename):
    wb = load_workbook(filename)
    ws = wb.active

    area_reg = r'(\d+),(\d+) кв\. м.{0,1}$'
    rooms_reg = r'^(\d)[-| ]{0,1}к'
    rc_reg = r'(ЖК «[^»]+»)'
    cost_reg = r'(\d+ \d+)'
    floor_reg = r'(^\d+$)|(^\d+-\d+$)'
    builder_reg = r'астройщик ([^»]+»)'

    res = []
    rcomplex = ""
    builder = ""
    area = 0
    rooms = 1
    for row in ws.iter_rows(min_row=2, max_col=10, max_row=2000, values_only=True):

        floor = 0
        cost = 0
        for r in row:
            r = str(r)
            _ = re.findall(rc_reg, r)
            if _:
                rcomplex = _[0]

            _ = re.findall(builder_reg, r)
            if _:
                builder = _[0]

            _ = re.findall(rooms_reg, r)
            if _:
                rooms = _[0]

            _ = re.findall(floor_reg, r)
            if _:
                floor = _[0][1] or _[0][0]

            _ = re.findall(cost_reg, r)
            if _:
                cost = toint(_[0])

            _ = re.findall(area_reg, r)
            if _:
                area = float(_[0][0]+"."+_[0][1])

        if not '0' in (str(cost), str(floor)):  # ячейка не пуста
            res.append(
                [
                    rcomplex,
                    builder,
                    rooms,
                    cost,
                    floor.split("-")[-1],
                    str(area)
                ]
            )
        else:
            pass

    return res


def is_same(a, b):
    a = copy(a)
    b = copy(b)
    a[3] = 0
    b[3] = 0
    return a == b


def calc_grow():
    new = load_from_xlsx("02.23.xlsx")
    old = load_from_xlsx("06.22.xlsx")
    vold = load_from_xlsx("01.22.xlsx")

    for n in new:
        o = [k for k in old if is_same(k, n)]
        vo = [k for k in vold if is_same(k, n)]
        grow = 0
        if vo:
            vo = vo[0][3]
        if o:
            o = o[0][3]
        if vo and o:
            print(n[3], o, vo)
            grow = round(n[3]/vo*100-100, 2)
            print(grow)
        n.append(grow)
    return new


def toint(st):
    return int(str(st).replace(" ", ""))


def get_ltd_info(search_str):
    r = requests.post("https://egrul.nalog.ru/", data="vyp3CaptchaToken=&page=&query="+search_str +
                      "&nameEq=on&region=23&PreventChromeAutocomplete=", headers={"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"})

    ts = round(time.time()*1000)
    res = requests.get("https://egrul.nalog.ru/search-result/" +
                       r.json()['t']+f"?r={ts}&_={ts+1}").json()
    print(res)
    return res['rows'][0]


def calc(rating, RC):
    K = 1.3  # Коэфициент зависимости спроса от рейтинга. Чем больше К тем больше спрос
    k = rating*K
    area_k = 1.5  # площадь/цену. Привемрно 1,5 кв.м. за 1000 рублей
    # простой 100% - 0 прибыли
    # простой 10% - -13% прибыли
    t = 0  # лет до окупания недвижимости
    data = {}
    for r in RC:
        area, cost, rooms, grow = r['area'], r['cost'], r['rooms'], r['grow'],
        repair_cost = 300_000+rooms*150_000  # траты на ремонт под ключ
        # аренлдная плата в месяц
        rate = area/area_k*1000*math.sqrt(k)*((100-grow)/100)
        t = (cost*area+repair_cost)/rate/12*((100-grow)/100)
        r['rate'] = rate

        data[t] = r
    res = []
    for _ in range(3):
        t = min(data.keys())
        d = data.pop(t)
        d['years'] = t
        res.append(d)
    return {"result": res}


def calc_all(RC, sum):
    complexes = db.RComplexes.all()
    K = 1.3  # Коэфициент зависимости спроса от рейтинга. Чем больше К тем больше спрос
    area_k = 1.5  # площадь/цену. Привемрно 1,5 кв.м. за 1000 рублей
    # простой 100% - 0 прибыли
    # простой 10% - -13% прибыли
    t = 0  # лет до окупания недвижимости
    data = {}
    for r in RC:
        k,rc_name = [[comp['rating'], comp['name']] for comp in complexes if comp['id'] == r['rcomplex']][0]
        k = k*K/100
        print(k)
        area, cost, rooms, grow = r['area'], r['cost'], r['rooms'], r['grow'],
        repair_cost = 300_000+rooms*150_000  # траты на ремонт под ключ
        # аренлдная плата в месяц
        rate = area/area_k*1000*math.sqrt(k)*((100-grow)/100)
        if cost*area+repair_cost < sum:
            t = (cost*area+repair_cost)/rate/12*((100-grow)/100)
            r['rc_name'] = rc_name
            r['rate'] = rate
            r['sum'] = cost*area+repair_cost

            data[t] = r
    res = []

    if len(data.keys())<3:
        return 404, {"error": "Слишком малый бюджет для инвестиций"}
    for _ in range(3):
        t = min(data.keys())
        d = data.pop(t)
        d['years'] = t
        res.append(d)
    return {"result": res}