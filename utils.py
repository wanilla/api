import hashlib
def dohash(input, len=64):
    return hashlib.sha256(input.encode()).hexdigest()[:len]
