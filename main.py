from db import User, Estate, RComplexes, Builder
import time
import utils
import sys
import apis
sys.path.append('/home/wex/dev/my/ehttp')
import ehttp
aHeaders = {
    "Access-Control-Allow-Origin": 'http://localhost:3000',
    'Access-Control-Allow-Credentials': "true",
    'Access-Control-Allow-Headers': '*'
}
OK = {"status": "ok"}

# TODO невозможность регистрации несколько на один номер телоефона


def gen_headers(req: ehttp.Request):
    aHeaders['Access-Control-Allow-Origin'] = req.headers.get("Origin")
    return aHeaders


server = ehttp.Server(force_bind=True, default_headers=gen_headers, port=3003)
auth_table = {}


def check_auth(req: ehttp.Request):

    user = auth_table.get(req.cookies.get("token"))

    if user:
        return user
    else:
        return ehttp.Response(401, {"error": "Невалидный токен доступа"})


@server.bind("/api/whoami", check_auth)
def whoami(req):
    return req.auth


@server.bind("/api/register")
def reg(request: ehttp.Request):  # TODO валидацуия и хендлинг ошибок
    User.reg(request.data["name"],
             request.data["phone"],
             request.data["email"],
             request.data["password"])
    return 200, OK, gen_headers(request)


@server.bind("/api/rcomplex/add")
def rcomplex_add(request: ehttp.Request):  # TODO валидацуия и хендлинг ошибок

    return 200, {"result": RComplexes.add(request.data["name"],
                                          request.data["photo"],
                                          request.data["builder"],
                                          request.data["rating"],
                                          request.data["lat"],
                                          request.data["lon"],
                                          request.data["addr"])}, gen_headers(request)


@server.bind("/api/estate/add")
def estate_add(request: ehttp.Request):

    return 200, {"result": Estate.add(request.data["rooms"],
                                      request.data["max_floor"],
                                      request.data["area"],
                                      request.data["cost"],
                                      request.data["rcomplex"], 0)}, gen_headers(request)


@server.bind("/api/oregister")
def oreg(request: ehttp.Request):  # TODO валидацуия и хендлинг ошибок
    Builder.reg(request.data["ogrn"],
                request.data["password"])
    return 200, Builder.reg(request.data["ogrn"],
                            request.data["password"]), gen_headers(request)


@server.bind("/api/auth")
def authorise(req: ehttp.Request):
    phone = req.data['phone']
    password = req.data['password']
    auth = User.auth(phone, password)
    if auth:
        token = utils.dohash(f"{phone} {password} {time.time_ns()}")
        auth_table[token] = auth
        return ehttp.Response(200, auth, gen_headers(req), cookies={"token": token})
    return ehttp.Response(401, {"error": "Неверный логин или пароль"})


@server.bind("/api/oauth")
def oauthorise(req: ehttp.Request):
    ogrn = req.data['ogrn']
    password = req.data['password']
    auth = Builder.auth(ogrn, password)
    if auth:
        token = utils.dohash(f"{ogrn} {password} {time.time_ns()}")
        auth_table[token] = auth
        return ehttp.Response(200, auth, gen_headers(req), cookies={"token": token})
    return ehttp.Response(401, {"error": "Неверный логин или пароль"})


@server.bind("/api/estate/all")
def all_estates(req):
    return {"result": Estate.all()}


@server.bind("/api/rcomplex/all")
def all_rcomplexes(req):
    return {"result": RComplexes.all()}


@server.bind("/api/calc")
def calc(req):
    id = req.data['id']
    return apis.calc(req.data['rating']/100, [e for e in Estate.all() if e['rcomplex'] == id])


@server.bind("/api/calc/all")
def calcall(req):
    sum = req.data['sum']
    return apis.calc_all(Estate.all(), sum)


@server.bind("/api/rcomplex/(\d+)")
def get_rcomplex(req, id):
    return [d for d in RComplexes.all() if d['id'] == int(id)][0]


server.start()
