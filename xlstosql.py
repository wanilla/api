import db
import apis
data = apis.calc_grow()
builders = {}
rcomplexes = {}
for da in data:
    rc_name = da[0]
    builder_name = da[1]
    if not builder_name in builders:
        builders[builder_name] = db.Builder.add(builder_name, 0)
    if not rc_name in rcomplexes:
        rcomplexes[rc_name] = db.RComplexes.add(
            rc_name,
            'photo',
            builders[builder_name],
            '100',
            'lat',
            'long',
            'addr')
    print(da)
    db.Estate.add(da[2],
                  da[4],
                  da[5],
                  da[3],
                  rcomplexes[rc_name],
                  da[6],
                  )
