# pylint: disable=missing-function-docstring,missing-class-docstring,missing-module-docstring

import pymysql
from cfg import db, password, user, logger, host, port
import utils
import apis

conn = pymysql.connect(host=host,
                       user=user,
                       password=password,
                       port=port,
                       database=db,
                       autocommit=True
                       )


def execute(cmd, *extra):
    global conn
    try:
        try:
            with conn.cursor() as cursor:
                logger.info("> %s", cmd)
                cursor.execute(cmd, extra)
                return cursor.fetchall()
        except (pymysql.err.OperationalError, AttributeError):
            raise pymysql.InterfaceError()
        except Exception as exception:
            raise exception
    except pymysql.InterfaceError:
        conn = pymysql.connect(host=host,
                               user=user,
                               password=password,
                               database=db,
                               autocommit=True
                               )
        return execute(cmd, *extra)
    except pymysql.err.InternalError:
        return execute(cmd, *extra)


class RComplexes:
    @staticmethod
    def add(name, photo, builder, rating, lat, lon, addr):
        execute("INSERT INTO rcomplexes (name, photo, builder, rating, lat, lon, addr) VALUES (%s, %s, %s, %s, %s, %s, %s)",
                name, photo, builder, rating, lat, lon, addr)
        return execute("select LAST_INSERT_ID()")[0][0]

    def all():
        res = []
        data = execute(
            "select id, name, photo, builder, rating, lat, lon, addr from rcomplexes")

        for d in data:
            res.append({
                "id": d[0],
                "name": d[1],
                "photo": d[2] or "https://static.vecteezy.com/system/resources/previews/011/651/324/original/newyork-city-highrise-skyline-simplicity-flat-design-free-png.png",
                "builder": d[3],
                "rating": d[4],
                "lat": d[5],
                "lon": d[6],
                "addr": d[7] or "Краснодар, улица пушкина, дом колотушкина"
            })
        return res


class User:
    @staticmethod
    def reg(name, phone, email, password):
        execute("INSERT INTO users (name, phone, email, password) VALUES (%s, %s, %s, %s)",
                name, phone, email, utils.dohash(password))

    @staticmethod
    def delete(phone):
        execute("DELETE FROM users WHERE phone=%s", phone)

    @staticmethod
    def auth(phone, password):
        raw_users = execute(
            "SELECT name, phone, email, money FROM users WHERE phone=%s and password=%s", phone, utils.dohash(password))
        if raw_users:
            return {
                "name": raw_users[0][0],
                "phone": raw_users[0][1],
                "email": raw_users[0][2],
                "money": raw_users[0][3]
            }


class Builder:
    @staticmethod
    def reg(ogrn, password):
        name = apis.get_ltd_info(ogrn)['c']
        execute("INSERT INTO builders (name, ogrn, password) VALUES (%s, %s, %s)",
                name, ogrn, utils.dohash(password))
        return {
            "id": Builder.add(name, 0),
            "name": name,
            "ogrn": ogrn
        }

    @staticmethod
    def auth(ogrn, password):
        raw_users = execute(
            "SELECT id, name, ogrn FROM builders WHERE ogrn=%s and password=%s", ogrn, utils.dohash(password))
        if raw_users:
            return {
                "id": raw_users[0][0],
                "name": raw_users[0][1],
                "ogrn": raw_users[0][2]
            }

    def add(name, verif):
        execute("INSERT INTO builders (name, verif) VALUES (%s, %s)", name, verif)
        return execute("select LAST_INSERT_ID()")[0][0]

    @staticmethod
    def all():
        data = execute("SELECT id, name, verif from builders")
        res = []
        for d in data:
            res.append({"id": d[0],
                        "name": d[1],
                        "verif": d[2]})

        return res


class Estate:
    @staticmethod
    def add(rooms, max_floor, area, cost, rcomplex, grow):
        execute("INSERT INTO estates (rooms, max_floor, area, cost, rcomplex, grow) VALUES (%s, %s,%s, %s, %s, %s)",
                rooms, max_floor, area, cost, rcomplex, grow)
        return execute("select LAST_INSERT_ID()")[0][0]

    @staticmethod
    def all():
        data = execute(
            "select id, rooms, max_floor, area, cost, rcomplex, grow from estates")
        res = []

        for d in data:
            res.append({
                "id": d[0],
                "rooms": d[1],
                "max_floor": d[2],
                "area": d[3],
                "cost": d[4],
                "grow": d[6],
                "rcomplex": d[5]})
        return res


if __name__ == "__main__":
    execute("""DROP TABLE IF EXISTS users;""")
    execute("""DROP TABLE IF EXISTS builders;""")
    execute("""DROP TABLE IF EXISTS estates;""")
    execute("""DROP TABLE IF EXISTS rcomplexes;""")

    execute("""CREATE TABLE users(
        name TEXT, 
        money INT default 0, 
        phone TEXT, 
        email TEXT,
        password TEXT
        );""")

    execute("""CREATE TABLE builders(
        id INT PRIMARY KEY AUTO_INCREMENT,
        name TEXT, 
        ogrn TEXT,
        password TEXT,
        verif INT
        );""")

    execute("""CREATE TABLE estates(
        id INT PRIMARY KEY AUTO_INCREMENT,
        rooms INT, 
        max_floor INT, 
        area FLOAT, 
        grow FLOAT default 0, 
        cost INT, 
        rcomplex INT
        );""")

    execute("""CREATE TABLE rcomplexes(
        id INT PRIMARY KEY AUTO_INCREMENT,
        name TEXT, 
        photo TEXT, 
        builder INT, 
        rating INT, 
        lat TEXT, 
        lon TEXT,
        addr TEXT
        );""")
